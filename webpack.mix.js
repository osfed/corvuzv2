const mix = require('laravel-mix');
mix.disableNotifications();

mix.browserSync({
    proxy: 'corvuzv2.test',
    port: 22066
});


/*****Login */
mix.stylus('resources/stylus/admin/login.styl', 'public/css/admin').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/admin/admin.styl', 'public/css/admin').version().options({ processCssUrls: false });


/*****Web */
mix.stylus('resources/stylus/home.styl', 'public/css').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/nosotros.styl', 'public/css').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/servicios.styl', 'public/css').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/contacto.styl', 'public/css').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/blog.styl', 'public/css').version().options({ processCssUrls: false });
mix.stylus('resources/stylus/blogDetalle.styl', 'public/css').version().options({ processCssUrls: false });

