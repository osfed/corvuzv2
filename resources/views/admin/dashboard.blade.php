@extends('admin.layout')


@section('contenido')

	<div class="Panel">
		<h1>Bienvenido</h1>
		<p>
			Sea usted bienvenido al administrador web de Corvuz. Este Panel de control cuenta con un eficiente sistema de navegación, el cual le facilitará el proceso administrativo que realice. Esperamos que su estadía en nuestro sistema sea grata.
			<br><br>
			A nombre de nuestra empresa, gracias por su preferencia.
		</p>
	</div>

@stop
