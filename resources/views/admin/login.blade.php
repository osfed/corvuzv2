<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Admin</title>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('/plugins/sweetalert/sweetalert.css') }}">
        <script src="{{ asset('/plugins/sweetalert/sweetalert.min.js') }}"></script>
        <link rel="stylesheet" href="{{ mix('/css/admin/login.css') }}">
        <link rel="shortcut icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
        <link rel="icon" href="{{ asset('/favicon.ico') }}" type="image/x-icon">
    </head>
    <body>

        <div class="Login-form">
    		<figure>
        		<img src="{{ asset('/images/corvuz.png') }}" alt="Corvuz">
        	</figure>
            <form action="{{ route('login') }}" class="Login-form-container" method="POST">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <div class="input text">
                    <label>Usuario</label>
                    <input type="email" name="email" required="" value="{{ old('email') }}">
                </div>
                <div class="input text">
                    <label>Contraseña</label>
                    <input type="password" name="password" required="" value="{{ old('password') }}">
                </div>
                <div>
                    <button class="btn btn--primary">Entrar</button>
                </div>
	        </form>
        </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <script type="text/javascript">
        	$(function(){
                @if(Session::get('alert'))
                    swal('', "{{Session::get('alert')['mensaje']}}", "{{Session::get('alert')['estatus']}}");
                @endif
        		$('.input.text input').on('blur', function(){
        			var $this = $(this);
        			if($this.val())
        			{
        				$this.addClass('used');
        			}
        		});

        		$('.input.text input').each(function(){
        			var $this = $(this);
        			if($this.val())
        			{
        				$this.addClass('used');
        			}
        		});
        	});
        </script>
    </body>
</html>
