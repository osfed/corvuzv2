@extends('admin.layout')


@section('contenido')

	<div class="Panel-titulo">
		<h1>Blogs</h1>
	</div>
	<div class="Panel-container Panel-container-view">
		<div class="Panel-container-view-actions ">
			<a class="btn btn--add" href="{{ route('blog.create') }}">
				Agregar
			</a>
		</div>
		@if($blog->count() > 0)
			<table class="table">
				<thead>
					<tr>
						<th>titulo</th>
						<th>keywords</th>
                        <th>imagen</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
					@foreach($blog as $element)
						<tr id="row-{{$element->id}}">
							<td>
								{{ $element->titulo }}
							</td>
							<td>
                                {{ $element->keywords }}
							</td>
                            <td>
                                <img class="imagen" src="{{ asset('/images/blog/'.$element->imagen)}}">
                            </td>
							<td class="Panel-actions">
								<a class="btn btn--primary" href="{{route('blog.edit',array($element->id))}}">
									Editar
								</a>
								<button class="btn btn--delete-no-data" data-url="{{route('blog.destroy',$element->id)}}">Eliminar</button>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		@else
			<p class="not-found">Aún no se han cargado los Blogs al sistema</p>
		@endif
	</div>

@stop

@section('js')
	<script type="text/javascript">
		$('#menu-blog').addClass('is-selected');
	</script>
@stop
