@extends('admin.layout')
@section('head')
	<link rel="stylesheet" href="{{ asset('/plugins/keywords/tagsinput.css') }}">
@endsection

@section('contenido')

	<div class="Panel-titulo">
		<h1>Editar blog</h1>
	</div>
	<div class="Panel">
		<div class="Panel-container-view-actions flex">
			<a class="btn btn--add" href="{{ route('blog.index') }}">
				Regresar
			</a>
		</div>
		<form action="{{route('blog.update',$blog->id)}}" method="POST" id="form" files="true" enctype="multipart/form-data">
			<input type="hidden" name="_token" value="{{csrf_token()}}">
			@method('PUT')
			<div class="input text">
				<label for="titulo">Titulo</label>
				<input type="text" name="titulo" value="{{$blog->titulo}}">
			</div>
			<div class="inputs text">
				<div class="input image">
                    <p>* Blog imagen (840x600)</p>
                    <div class="container">
                        <i class="icon icon-close">x</i>
                        <span class="btn btn--save">Seleccionar archivo (jpg, png)</span>
                        <input type="file" data-img="imagen_movil" name="imagen" id="data-imagen_movil" class="input-file"  accept="image/x-png,image/jpeg">
                        <div class="imagen">
							<img id="img_imagen_movil" class="is-visible"
							src="{{asset('/images/blog/'.$blog->imagen)}}" alt="CORVUZ"/>
                        </div>
                    </div>
                </div>
				<div class="input text">
					<label for="keywords">Keywords</label>
					<input type="text" id="keywords" name="keywords" value="{{$blog->keywords}}">
				</div>
			</div>
			<div class="inputs text">
				<div class="input text">
					<label for="intro">Intro</label>
					<textarea name="intro" id="intro" cols="30" rows="10">{{$blog->intro}}</textarea>
				</div>
				<div class="input text">
					<label for="description">Descripcion</label>
					<textarea name="description" id="description" cols="30" rows="10">{{$blog->description}}</textarea>
				</div>
			</div>
            <div class="inputs text">
                <div class="input text">
                    <label for="post">Post</label>
                    <input type="text" name="post" value="{{$blog->post}}">
                </div>
                <div class="input text">
                    <label for="activo">Activo</label>
                    <input type="text" name="activo" value="{{$blog->activo}}">
                </div>
            </div>

			<div class="button">
				<button class="btn btn--save btn--large">Guardar</button>
			</div>
		</form>
	</div>
@stop

@section('js')
    <script src="{{ asset('/plugins/keywords/tagsinput.js') }}"></script>
	<script type="text/javascript">
		$(function(){
			$('#menu-blog').addClass('is-selected');

            $('#description').trumbowyg({
                removeformatPasted: true
            });
            $('#intro').trumbowyg({
                removeformatPasted: true
            });
            $('#keywords').tagsInput({
				placeholder:'Agregar keyword'
			});
        });
    </script>
@stop
