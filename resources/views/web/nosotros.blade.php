@extends('web.layout')
@section('head')
		<title>Nosotros -Corvuz</title>
        <link rel="stylesheet" type="text/css" href="plugins/slick/slick.css"/>
        <link rel="stylesheet" type="text/css" href="plugins/slick/slick-theme.css"/>
		<link rel="stylesheet" href="{{ mix('/css/nosotros.css') }}">
@endsection
@section('contenido')
		<div class="Nosotros">
            <div class="Nosotros-banner">
                <div class="layout Nosotros-banner-container">
                    <div class="Nosotros-banner-imagen">
                        <img src="images/us/grafico.png" alt="CORVUZ">
                    </div>
                    <div class="Nosotros-banner-texto">
                        <h1>Nos <strong>actualizamos</strong> cada día para poder estar a la par de las <strong>mejores</strong> agencias de la región</h1>
                        <p>Somos una agencia multidiciplinaria, conformada por expertos en programación, diseño y recientemente marketing digital.</p>
                    </div>
                </div>
            </div>
            <div class="Nosotros-software">
                <div class="layout Nosotros-software-container">
                    <div class="Nosotros-software-logo">
                        <img src="images/us/logo_flutter.png" alt="CORVUZ">
                    </div>
                    <div class="Nosotros-software-logo">
                        <img src="images/us/logo_mysql.png" alt="CORVUZ">
                    </div>
                    <div class="Nosotros-software-logo">
                        <img src="images/us/logo_v.png" alt="CORVUZ">
                    </div>
                    <div class="Nosotros-software-logo">
                        <img src="images/us/logo_laravel.png" alt="CORVUZ">
                    </div>
                    <div class="Nosotros-software-logo">
                        <img src="images/us/logo_stylus.png" alt="CORVUZ">
                    </div>
                </div>
            </div>
            <div class="Nosotros-buscamos">
                <p>Buscamos estar siempre a la <strong>vanguardia</strong></p>
            </div>
            <div class="verticalScroller">
                <div class="layout verticalScroller-container">
                    <div class="verticalScroller-izq">
                        <div class="verticalScroller-titulo">
                            <h2>Nuestra metodología</h2>
                            <p>Con cada proyecto vamos mejorando nuestra manera
                                de trabajar para lograr proyectos más efectivos</p>
                        </div>
                        <div class="slider-nav">
                            <div class="texto">
                                <h2><span>01.</span> Análisis</h2>
                                <ul class="submenu">
                                    <li>
                                        · Recopilación de información
                                    </li>
                                    <li>
                                        · Definir objetivos
                                    </li>
                                    <li>
                                        · Definir públicos
                                    </li>
                                    <li>
                                        · Planificación de contenido
                                    </li>
                                    <li>
                                        · Cronograma
                                    </li>
                                </ul>
                            </div>
                            <div class="texto">
                                <h2><span>02.</span> Diseño y validación</h2>
                                <ul class="submenu">
                                    <li>
                                        · Propuesta de diseño
                                    </li>
                                    <li>
                                        · Revisión
                                    </li>
                                    <li>
                                        · Validación de cliente
                                    </li>
                                    <li>
                                        · Ajustes (en caso de requerir)
                                    </li>
                                </ul>
                            </div>
                            <div class="texto">
                                <h2><span>03.</span> Desarrollo e implementación</h2>
                                <ul class="submenu">
                                    <li>
                                        · Programación
                                    </li>
                                    <li>
                                        · Puebas
                                    </li>
                                    <li>
                                        · Validación
                                    </li>
                                    <li>
                                        · Lanzamiento
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                   <div class="verticalScroller-imagen">
                       <div class="slider-for">
                        <div>
                            <img src="images/us/analisis.png" alt="CORVUZ">
                        </div>
                        <div>
                            <img src="images/us/diseño_validación.png" alt="CORVUZ">
                        </div>
                        <div>
                            <img src="images/us/desarrollo_e_implementación.png" alt="CORVUZ">
                        </div>
                       </div>
                   </div>
                </div>
            </div>
            <div class="conexion">
                <div class="layout conexion-container">
                    <div class="conexion-imagen">
                        <img src="images/home/gif3.gif" alt="CORVUZ">
                    </div>
                    <div class="conexion-texto">
                        <div class="textos">
                            <h3>Conexiones a otras plataformas</h3>
                            <p>Integramos distinta plataformas a tu sitio web, sistema o ecommerce a través de API's, como facturación, marketplaces, seguros de contracargo, sistemas de inventarios entre otros.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="clientes">
                <div class="layout">
                    <div class="clientes-titulo">
                        <h2>Nuestros clientes son la mejor referencia de nuestro trabajo</h2>
                        <p>¡Gracias por confiar en nosotros!</p>
                    </div>
                    <div class="clientes-items">
                        <a href="https://www.danteshoes.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/dante.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.quirelli.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/quirelli.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.epca.edu.mx/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/epca.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.pateywoman.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/patey_woman.png')}}" alt="CORVUZ"></a>
                        <a href="http://www.ianndey.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/ianndey.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.lasallemorelia.edu.mx/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/la_salle.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.chilesselectosdelbajio.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/logo_chiles.png')}}" alt="CORVUZ"></a>
                        <a href="https://tacospapis.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/logo_tacos.png')}}" alt="CORVUZ"></a>
                    </div>
                </div>
            </div>
		</div>
@endsection
@section('js')

        <script type="text/javascript" src="plugins/slick/slick.js"></script>
        <script type="text/javascript">
            $(function() {
                $('.subtitulo').click(function() {
                    if($('.subtitulo').hasClass('active')){
                        $('.subtitulo').removeClass('active');
                        $('.content').not('#content' + $(this).attr('target')).hide();
                        $('#content' + $(this).attr('target')).show();
                        if($('.subtitulo').next().show()){
                            $('.subtitulo').next().hide();
                        }
                    }
                    $(this).addClass('active');
                    $(this).next().slideDown(500);

                });
            });
            $(function(){
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    speed: 500,
                    asNavFor: '.slider-nav'
                });
                $('.slider-nav').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    asNavFor: '.slider-for',
                    arrows: false,
                    dots: true,
                    vertical: true,
                    verticalSwiping: true,
                    responsive: [
                        {
                            breakpoint: 1030,
                            settings: {
                                vertical: false,
                                verticalSwiping: false
                            }
                        }
                    ]
                });
                $('.slider-nav').on('wheel', (function(e) {
                    e.preventDefault();

                    clearTimeout(scroll);
                    scroll = setTimeout(function(){scrollCount=0;}, 200);
                    if(scrollCount) return 0;
                    scrollCount=1;

                    if (e.originalEvent.deltaY < 0) {
                        $(this).slick('slickPrev');
                    } else {
                        $(this).slick('slickNext');
                    }
                }));
            });
        </script>
@endsection
