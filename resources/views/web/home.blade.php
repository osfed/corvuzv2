@extends('web.layout')
@section('head')
    <title>Corvuz</title>
    <link rel="stylesheet" href="{{ mix('/css/home.css') }}">
@endsection
@section('contenido')
		<div class="home">
			<div class="home-banner">
				<div class="layout home-banner-info">
					<div class="home-banner-info-titulo">
						<h1>Conviértete en una <span>referencia</span> para <br>tus competidores</h1>
						<p>
							Tenemos sed de crecimiento, por lo que cada día nos preparamos más,
							para así poder ofrecer a nuestros clientes todo lo que necesiten para
							su propio desarrollo, desde un sitio web o sistemas completos hasta una APP.
						</p>
					</div>
					<div class="home-banner-info-imagen">
						<img src="images/home/celulares.png" alt="CORVUZ">
					</div>
				</div>
			</div>
            <div class="clientes">
                <div class="layout">
                    <div class="clientes-titulo">
                        <h2>Nuestros clientes son la mejor referencia de nuestro trabajo</h2>
                        <p>¡Gracias por confiar en nosotros!</p>
                    </div>
                    <div class="clientes-items">
                        <a href="https://www.danteshoes.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/dante.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.quirelli.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/quirelli.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.epca.edu.mx/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/epca.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.pateywoman.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/patey_woman.png')}}" alt="CORVUZ"></a>
                        <a href="http://www.ianndey.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/ianndey.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.lasallemorelia.edu.mx/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/la_salle.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.chilesselectosdelbajio.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/logo_chiles.png')}}" alt="CORVUZ"></a>
                        <a href="https://tacospapis.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/logo_tacos.png')}}" alt="CORVUZ"></a>
                    </div>
                </div>
            </div>
			<div class="layout">
                <div class="home-proyectos">
                    <h3>Proyectos</h3>
                    <div class="home-proyectos-conexion">
                        <div class="home-proyectos-conexion-50">
                            <img src="images/home/gif1.gif" alt="CORVUZ">
                        </div>
                        <div class="home-proyectos-conexion-50 text">
                            <div class="info">
                                <h2>Conexión de inventario a <span>Liverpool Marketplace</span></h2>
                                <p>
                                    Calzado DANTE buscaba conectarse al marketplace de Liverpool sin la
                                    necesidad de reservar un inventario especial para el mismo.
                                    <br>
                                    <br>
                                    Se necesitaba desarrollar la forma de sincronizar el inventario del
                                    e-commerce de Calzado Dante a la plataforma que usa Liverpool.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="home-servicios">
                    <div class="home-servicios-titulo">
                        <h2>Nuestros Servicios</h2>
                        <p>Tenemos justo lo que necesitas</p>
                    </div>
                    <div class="home-servicios-contenedor">
                        <a href="/servicios#Diseño" class="contenedores">
                            <div class="img">
                                <img src="images/home/diseño_digital.png" alt="CORVUZ">
                                <p>DISEÑO DIGITAL</p>
                            </div>
                        </a>
                        <a href="/servicios#Sitios" class="contenedores">
                            <div class="img">
                                <img src="images/home/sitios_web.png" alt="CORVUZ">
                                <p>SITIOS WEB</p>
                            </div>
                        </a>
                        <a href="/servicios#Ecommerce" class="contenedores col">
                            <div class="img">
                                <img src="images/home/ecommerce.png" alt="CORVUZ">
                                <p>ECOMMERCE</p>
                            </div>
                        </a>
                        <a href="/servicios#Sistemas" class="contenedores">
                            <div class="img">
                                <img src="images/home/sistemas.png" alt="CORVUZ">
                                <p>SISTEMAS</p>
                            </div>
                        </a>
                        <a href="/servicios#Redes" class="contenedores">
                            <div class="img">
                                <img src="images/home/redes_sociales.png" alt="CORVUZ">
                                <p>REDES SOCIALES</p>
                            </div>
                        </a>
                        <a href="/servicios#Apps" class="contenedores col">
                            <div class="img">
                                <img src="images/home/apps.png" alt="CORVUZ">
                                <p>APPS</p>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <div class="pagos">
                <div class="layout pagos-lineas">
                    <div class="pagos-lineas-texto">
                        <div class="texto">
                            <h3>Pagos en línea</h3>
                            <p>Te ayudamos a implementar distintas pasarelas de pago en tu ecommerce o sistema para que tus clientes tengan varias opciones para concluir sus compras.</p>
                        </div>
                    </div>
                    <div class="pagos-lineas-imagen">
                        <img src="images/home/gif2.gif" alt="CORVUZ">
                    </div>
                </div>
            </div>
            <div class="conexion">
                <div class="layout conexion-container">
                    <div class="conexion-imagen">
                        <img src="images/home/gif3.gif" alt="CORVUZ">
                    </div>
                    <div class="conexion-texto">
                        <div class="textos">
                            <h3>Conexiones a otras plataformas</h3>
                            <p>Integramos distinta plataformas a tu sitio web, sistema o ecommerce a través de API's, como facturación, marketplaces, seguros de contracargo, sistemas de inventarios entre otros.</p>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="home-blog">
                <div class="layout">
                    <h2>Descubre artículos utiles en nuestro blog</h2>
                    <div class="articulos">
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                    </div>
                </div>
            </div> --}}

		</div>
@endsection
@section('js')
@endsection
