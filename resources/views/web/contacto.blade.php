@extends('web.layout')
@section('head')
		<title>Contacto -Corvuz</title>
		<link rel="stylesheet" href="plugins/validationEngine/validationEngine.jquery.css" />
		<link rel="stylesheet" href="{{ mix('/css/contacto.css') }}">
@endsection
@section('contenido')
		<div class="contacto">
			<div class="layout">
				<div class="contacto-comunicate">
					<div class="contacto-comunicate-50">
						<div class="texto">
							<h1>Platiquemos sobre <span>tu proyecto</span></h1>
							<p>Contáctanos y háblanos más sobre tu proyecto, para poder pensar en el mejor desarrollo de acuerdo a tus necesidades y ¡lograr el éxito!</p>
						</div>
					</div>
					<div class="contacto-comunicate-50">
						<div class="fondo">
							<div class="fondo-formulario">
								<form id="formContacto" method="post" action="#">
									<div class="input">
										<label>Nombre</label><br>
										<input type="text" name="Nombre" class="validate[required]">
									</div>
									<div class="input">
										<label>Correo</label><br>
										<input type="email" name="Correo" class="validate[required]">
									</div>
									<div class="input">
										<label>Teléfono</label><br>
										<input type="text" name="Teléfono" class="validate[required]">
									</div>
									<div class="input">
										<label>Empresa</label><br>
										<input type="text" name="Empresa" class="validate[required]">
									</div>
									<label>Elige una opción</label><br>
									<div class="fondo-formulario-opciones">
										<span class="opcion">
                                            <input type="radio" id="diseño" name="opcion" value="diseño">
                                            <img src="images/contacto/diseño_digital.png" alt="CORVUZ">
                                            <p>DISEÑO DIGITAL</p>
										</span>
										<span class="opcion">
                                            <input type="radio" id="sitio" name="opcion" value="sitio">
                                            <img src="images/contacto/sitios_web.png" alt="CORVUZ">
                                            <p>SITIOS WEB</p>
										</span>
										<span class="opcion">
                                            <input type="radio" id="ecommerce" name="opcion" value="ecommerce">
                                            <img src="images/contacto/ecommerce.png" alt="CORVUZ">
                                            <p>ECOMMERCE</p>
										</span>
										<span class="opcion">
                                            <input type="radio" id="sistema" name="opcion" value="sistema">
                                            <img src="images/contacto/sistemas.png" alt="CORVUZ">
                                            <p>SISTEMAS</p>
										</span>
										<span class="opcion">
                                            <input type="radio" id="redesSociales" name="opcion" value="redes">
                                            <img src="images/contacto/redes_sociales.png" alt="CORVUZ">
                                            <p>REDES SOCIALES</p>
										</span>
										<span class="opcion">
                                            <input type="radio" id="app" name="opcion" value="app">
                                            <img src="images/contacto/apps.png" alt="CORVUZ">
                                            <p>APPS</p>
										</span>
									</div>
									<label>Cuéntanos sobre tu proyecto</label><br>
									<div class="input">
										<textarea name="proyecto" class="validate[required]"></textarea>
									</div>
									<div class="recaptcha">

									</div>
									<div class="boton">
										<a href="#">Enviar</a>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
@endsection
@section('js')
		<script src="plugins/validationEngine/jquery.validationEngine-es.js"></script>
		<script src="plugins/validationEngine/jquery.validationEngine.js"></script>
		<script>
			$(function(){
				$('#formContacto').validationEngine({
					scroll: false
				});
			});
            $('.opcion').on('click', function(){
                if($('.opcion').hasClass('selected'))
                {
                    $('.opcion').removeClass('selected');
                }
                $(this).addClass('selected');
            });
		</script>
@endsection
