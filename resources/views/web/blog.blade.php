@extends('web.layout')
@section('head')
        <title>Blog -Corvuz</title>
        <link rel="stylesheet" href="{{ mix('/css/blog.css') }}">
@endsection
@section('contenido')
        <div class="Blog">
            <div class="Blog-banner">
                <div class="layout Blog-banner-container">
                    <div class="Blog-banner-imagen">
                        <img src="images/blog/grafico.png" alt="CORVUZ">
                    </div>
                    <div class="Blog-banner-texto">
                        <h1>Blog <strong>relevante</strong></h1>
                        <p>Somos una agencia multidiciplinaria, conformada por expertos en programación, diseño y recientemente marketing digital.</p>
                    </div>
                </div>
            </div>
            <div class="Blog-contenido">
                <div class="layout">
                    <div class="Blog-contenido-menu">
                        <div class="Blog-contenido-buscador">
                            <h2>¿Qué estas buscando?</h2>
                            <div class="barra">
                                <img src="images/blog/buscador.png" alt="CORVUZ">
                                <input type="text">
                            </div>
                        </div>
                        <div class="Blog-contenido-categorias">
                            <ul>
                                <li class="categoria">Diseño Gráfico</li>
                                <li class="categoria">Web</li>
                                <li class="categoria">Marketplace</li>
                                <li class="categoria">Apps</li>
                                <li class="categoria">Proyectos</li>
                            </ul>
                        </div>
                    </div>
                    <div class="Blog-contenido-items">
                        @foreach ($blog as $item)
                        <a href="blog/{{$item->slug}}" class="blogItem">
                            <img src="images/blog/{{$item->imagen}}" alt="{{$item->titulo}}">
                            <h4>{{$item->titulo}}</h4>
                            <p>{!!$item->intro!!}</p>
                            <span>Ver más</span>
                        </a>
                        @endforeach
                    </div>
                    {{-- <div class="Blog-contenido-items">
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                        <a  href="{{ route('blogDetalle')}}"class="blogItem">
                            <img src="images/home/blog_imagen.png" alt="">
                            <h4>Supernova a design system platform</h4>
                            <p>Lorem ipsum dolor sit amet, consectetuer elit. Aenean commodo ligula eget dolor...</p>
                            <span>Ver más</span>
                        </a>
                    </div> --}}
                </div>
            </div>
        </div>
@endsection
@section('js')
        <script type="text/javascript">
            $(function() {
                $('.subtitulo').click(function() {
                    $('.content').not('#content' + $(this).attr('target')).hide();
                    $('#content' + $(this).attr('target')).toggle();
                    $(this).adClass('active').siblings().removeClass('active');
                    $('submenu').toggle();
                });
                $('.categoria').click(function() {
                    if($('.categoria').hasClass('active')){
                        $('.categoria').removeClass('active');
                    }
                    $(this).addClass('active');
                })
            });
        </script>
@endsection
