@extends('web.layout')
@section('head')
	<title>Blog Detalle -Corvuz</title>
	<link rel="stylesheet" href="{{ mix('/css/blogDetalle.css') }}">
@endsection
@section('contenido')
    <div class="detalle">
        <div class="layout detalle-container">
            <div class="articulo">
                <img src="{{asset('images/blog/'.$blogDetalle->imagen)}}" alt="{{$blogDetalle->titulo}}">
                <h1 id="Titulo">{{$blogDetalle->titulo}}</h1>
                {!!$blogDetalle->description!!}
            </div>
            <div class="detalle-buscador">
				<div class="barra">
					<div class="barra-resultados">
						<h2>Más artículos</h2>
                        @foreach ($ultimos as $item)
						<a href="{{$item->slug}}">{{$item->titulo}}</a>
                        @endforeach
					</div>
				</div>
			</div>
        </div>
    </div>
@endsection
