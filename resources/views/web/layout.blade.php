<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
	@yield('head')
</head>

<body>
    <header class="header">
        <div class="layout header-container">
            <figure>
                <a href="{{ route('home') }}">
                    <img src="{{asset('images/corvuz_blanco.png')}}" alt="CORVUZ">
                </a>
            </figure>
            <span id="menu" class="icon icon-menu"></span>
            <nav class=menu>
                <ul>
                    <li id="menu-home">
                        <a href="{{ route('home')}}">Home</a>
                    </li>
                    <li id="menu-nosotros">
                        <a href="{{ route('nosotros')}}">Nosotros</a>
                    </li>
                    <li id="menu-servicios">
                        <a href="{{ route('servicios')}}">Servicios</a>
                    </li>
                    {{-- <li>
                        <a href="{{ route('blog')}}">Blog</a>
                    </li> --}}
                    <li id="menu-contacto">
                        <a href="{{ route('contacto')}}">Contacto</a>
                    </li>
                </ul>
            </nav>
        </div>
    </header>
	@yield('contenido')
    <footer class="footer">
        <div class="layout footer-container">
            <figure>
                <a href="{{ route('home') }}">
                    <img src="{{asset('images/corvuz_blanco.png')}}" alt="CORVUZ">
                </a>
            </figure>
            <div class="footer-menu">
                <ul>
                    <li id="home">
                        <a href="{{ route('home')}}">Home</a>
                    </li>
                    <li>
                        <a href="{{ route('nosotros')}}">Nosotros</a>
                    </li>
                    <li>
                        <a href="{{ route('servicios')}}">Servicios</a>
                    </li>
                    {{-- <li>
                        <a href="{{ route('blog')}}">Blog</a>
                    </li> --}}
                </ul>
            </div>
            <div class="footer-contacto">
                <h3>Contáctanos</h3>
                <p>476 113 3570</p>
                <p>hola@corvuz.com</p>
                <p>
                    16 de septiembre #223 Zona Centro
                    <br>
                    Purísima del Rincón, Gto.
                </p>
            </div>
            <div class="footer-siguenos">
                <h3>Síguenos</h3>
                <div class="redes">
                    <a href="https://www.instagram.com/corvuzweb" target="_blank">
                        <img src="{{asset('images/instagram.png')}}" alt="CORVUZ">
                    </a>
                    <a href="https://facebook.com/corvuzweb/" target="_blank">
                        <img src="{{asset('images/facebook.png')}}" alt="CORVUZ">
                    </a>
                </div>
                {{-- <a href="#">Aviso de privacidad</a> --}}
            </div>
        </div>
    </footer>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="plugins/jquery.min.js"></script>
    <script>
        $(function(){
            $('#menu').on('click', function () {
				$('.menu').toggleClass('show');
				$(this).toggleClass('show');
                $('.header figure').toggleClass('show');
                $('.logoBlanco').toggleClass('show');
                $('.logoNegro').toggleClass('show');

			});
        });
    </script>
	@yield('js')
</body>
</html>
