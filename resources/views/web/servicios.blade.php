@extends('web.layout')
@section('head')
		<title>Servicios -Corvuz</title>
		<link rel="stylesheet" href="{{ mix('/css/servicios.css') }}">
@endsection
@section('contenido')
		<div class="Servicios">
            <div class="Servicios-banner">
                <div class="layout Servicios-banner-container">
                    <div class="Servicios-banner-imagen">
                        <img src="images/servicios/imagen.png" alt="CORVUZ">
                    </div>
                    <div class="Servicios-banner-texto">
                       <h1>Tenemos justo lo que <strong>necesitas</strong></h1>
                    </div>
                </div>
            </div>
            <div class="layout Servicios-items">
                <div class="servicio" id="Diseño">
                    <div class="servicio-imagen">
                        <img src="images/servicios/diseño.png" alt="CORVUZ">
                    </div>
                    <div class="servicio-texto">
                        <h2>Diseño digital</h2>
                        <p>Con propuestas innovadoras, ayudamos a que tu sistema, APP o web además de ser visualmente atractiva, tenga funcionalidad y que un proceso complejo se pueda resolver en pocos clicks.</p>
                        <ul>
                            <li>
                                Web
                            </li>
                            <li>
                                Apps
                            </li>
                            <li>
                                Ux
                            </li>
                            <li>
                                Redes Sociales
                            </li>
                            <li>
                                Branding
                            </li>
                            <li>
                                Fotografía
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="servicio2" id="Sitios">
                    <div class="servicio2-texto">
                        <h2>Sitios web</h2>
                        <p>Te ayudamos a generar el Sitio Web perfecto para tu negocio, donde podrás compartir a tus clientes todo lo que deben conocer sobre tu empresa y a que te contacten nuevos.</p>
                        <ul>
                            <li>
                                Sitios web
                            </li>
                            <li>
                                Landing page
                            </li>
                            <li>
                                Administrador de contenido
                            </li>
                        </ul>
                    </div>
                    <div class="servicio2-imagen">
                        <img src="images/servicios/sitios_web.png" alt="CORVUZ">
                    </div>
                </div>
                <div class="servicio" id="Apps">
                    <div class="servicio-imagen">
                        <img src="images/servicios/apps.png" alt="CORVUZ">
                    </div>
                    <div class="servicio-texto">
                        <h2>APPS</h2>
                        <p>Tener una APP para tu negocio, puede ser una herramienta adicional que ayudará a sistematizar o automatizar procesos, mantener clientes activos y tener una mayor cercanía con ellos.</p>
                        <ul>
                            <li>
                                ¡Desarrollamos Apps de acuerdo a tus necesidades!
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="servicio2" id="Ecommerce">
                    <div class="servicio2-texto">
                        <h2>Ecommerce</h2>
                        <p>Comprar en línea, es lo de hoy. Facilita la vida a tus clientes y ofréceles un sitio donde podrán encontrar todos tus productos al alcance de un clic y con las pasarelas de pago más eficientes de las ventas online.</p>
                        <ul>
                            <li>
                                Tienda online
                            </li>
                            <li>
                                Pasarelas de pago
                            </li>
                            <li>
                                Control de inventarios
                            </li>
                        </ul>
                    </div>
                    <div class="servicio2-imagen">
                        <img src="images/servicios/ecommerce.png" alt="CORVUZ">
                    </div>
                </div>
                <div class="servicio" id="Sistemas">
                    <div class="servicio-imagen">
                        <img src="images/servicios/sistemas.png" alt="CORVUZ">
                    </div>
                    <div class="servicio-texto">
                        <h2>Sistemas</h2>
                        <p>Manejamos diferentes roles para desarrollar proyectos tanto sencillos como complejos de manera ágil.</p>
                        <ul>
                            <li>
                                Sistemas monoliticos
                            </li>
                            <li>
                                Microservicios
                            </li>
                            <li>
                                API's
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="servicio2" id="Redes">
                    <div class="servicio2-texto">
                        <h2>Redes sociales</h2>
                        <p>Generamos estrategias para Redes Sociales, a través del análisis del sector, la empresa y las tendencias.</p>

                    </div>
                    <div class="servicio2-imagen">
                        <img src="images/servicios/redes.png" alt="CORVUZ">
                    </div>
                </div>
            </div>
            <div class="pagos">
                <div class="layout pagos-lineas">
                    <div class="pagos-lineas-texto">
                        <div class="texto">
                            <h3>Pagos en línea</h3>
                            <p>Te ayudamos a implementar distintas pasarelas de pago en tu ecommerce o sistema para que tus clientes tengan varias opciones para concluir sus compras.</p>
                        </div>
                    </div>
                    <div class="pagos-lineas-imagen">
                        <img src="images/home/gif2.gif" alt="CORVUZ">
                    </div>
                </div>
            </div>
            <div class="conexion">
                <div class="layout conexion-container">
                    <div class="conexion-imagen">
                        <img src="images/home/gif3.gif" alt="CORVUZ">
                    </div>
                    <div class="conexion-texto">
                        <div class="textos">
                            <h3>Conexiones a otras plataformas</h3>
                            <p>Integramos distinta plataformas a tu sitio web, sistema o ecommerce a través de API's, como facturación, marketplaces, seguros de contracargo, sistemas de inventarios entre otros.</p>
                        </div>
                    </div>
                </div>
            </div>
            <span class="espacio"></span>
            <div class="clientes">
                <div class="layout">
                    <div class="clientes-titulo">
                        <h2>Nuestros clientes son la mejor referencia de nuestro trabajo</h2>
                        <p>¡Gracias por confiar en nosotros!</p>
                    </div>
                    <div class="clientes-items">
                        <a href="https://www.danteshoes.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/dante.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.quirelli.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/quirelli.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.epca.edu.mx/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/epca.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.pateywoman.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/patey_woman.png')}}" alt="CORVUZ"></a>
                        <a href="http://www.ianndey.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/ianndey.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.lasallemorelia.edu.mx/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/la_salle.png')}}" alt="CORVUZ"></a>
                        <a href="https://www.chilesselectosdelbajio.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/logo_chiles.png')}}" alt="CORVUZ"></a>
                        <a href="https://tacospapis.com/" class="clientes-logo" target="_blank"><img src="{{asset('images/home/logo_tacos.png')}}" alt="CORVUZ"></a>
                    </div>
                </div>
            </div>
            <span class="espacio"></span>
        </div>
@endsection
