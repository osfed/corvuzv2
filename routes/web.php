<?php

use Illuminate\Support\Facades\Route;


Route::get('/', 'WebController@index')->name('home');
Route::match(['get', 'post'], '/contacto', 'WebController@contacto')->name('contacto');
Route::match(['get', 'post'], '/nosotros', 'WebController@nosotros')->name('nosotros');
Route::match(['get', 'post'], '/servicios', 'WebController@servicios')->name('servicios');
Route::match(['get', 'post'], '/blog', 'WebController@blog')->name('blog');
Route::match(['get', 'post'], '/blog/{id}', 'WebController@blogDetalle')->name('blogDetalle');
Route::match(['get', 'post'], '/us_analisis', 'WebController@us_analisis')->name('us_analisis');


Route::match(['GET', 'POST'], '/admin', 'Admin\AccesoController@login')->name('login');

$admin = ['prefix' => 'panel', 'namespace' => 'Admin', 'middleware' => 'auth'];
Route::group($admin, function() {

    $ctrl = 'AccesoController';
	$as = 'admin';
    Route::get('/dashboard', "$ctrl@dashboard")->name($as.'_dashboard');
    Route::get('/salir', "$ctrl@salir")->name($as.'_salir');

    Route::resource('blog', 'BlogController');
});
