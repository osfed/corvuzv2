<?php

namespace App\Utils;

use App\Utils\Utils;
use App\Models\Orden;
use App\Models\Productos;
use App\Models\Configuracion;

class Utils
{

	public function doURL($val)
	{
		if(trim($val) != null)
		{
			$url = $val;
		    if (!preg_match("~^(?:f|ht)tps?://~i", $url)) {
		        $url = "http://" . $url;
		    }
		    return $url;
		}
		return null;
	}

	public function doURLYoutube($url)
	{
	    $pattern = '#^(?:https?://|//)?(?:www\.|m\.)?(?:youtu\.be/|youtube\.com/(?:embed/|v/|watch\?v=|watch\?.+&v=))([\w-]{11})(?![\w-])#';
		preg_match($pattern, $url, $matches);
		return (isset($matches[1])) ? $matches[1] : '';
	}

	public function doSlug($val)
	{
		$text = $val;
		$text = preg_replace('~[^\\pL\d]+~u', '-', $text);
		$text = trim($text, '-');
		$text = str_replace( array('à','á','â','ã','ä', 'ç', 'è','é','ê','ë', 'ì','í','î','ï', 'ñ', 'ò','ó','ô','õ','ö', 'ù','ú','û','ü', 'ý','ÿ', 'À','Á','Â','Ã','Ä', 'Ç', 'È','É','Ê','Ë', 'Ì','Í','Î','Ï', 'Ñ', 'Ò','Ó','Ô','Õ','Ö', 'Ù','Ú','Û','Ü', 'Ý'), array('a','a','a','a','a', 'c', 'e','e','e','e', 'i','i','i','i', 'n', 'o','o','o','o','o', 'u','u','u','u', 'y','y', 'A','A','A','A','A', 'C', 'E','E','E','E', 'I','I','I','I', 'N', 'O','O','O','O','O', 'U','U','U','U', 'Y'), $text);
		$text = strtolower($text);
		$text = preg_replace('~[^-\w]+~', '', $text);
		return $text;
	}

	public function doHashtag($text)
	{
		$text = preg_replace('/#(\w+)/', ' <a href="https://www.instagram.com/explore/tags/$1">#$1</a>', $text);
		return $text;
	}

	public function cancelarOrdenes($id){

		\DB::beginTransaction();
		$data = array('estatus' => 200);
		try {

            $orden = Orden::with('productos')->where('id', $id)->first();
            $orden->estatus = 'cancelada';
            $orden->save();

            foreach ($orden->productos as $key => $producto)
            {
            	$productos = Productos::where('id', $producto->producto_id)
            	->first();

            	if($productos != null)
            	{
            		$productos->stock = $productos->stock+$producto->cantidad;
            		$productos->save();
            	}
            }

            \DB::commit();
        } catch (\Exception $e) {
        	\DB::rollBack();
        	$data = array('estatus' => 501, 'mensaje' => $e->getMessage(), 'line' => $e->getLine());
        }
        return $data;
	}

	public function expirarOrdenes($id){
		\DB::beginTransaction();
		$data = array('estatus' => 200);
		try {

            $orden = Orden::with('productos')->where('id', $id)->first();
            $orden->estatus = 'expiró';
            $orden->save();

            foreach ($orden->productos as $key => $producto)
            {
            	$productos = Productos::where('id', $producto->producto_id)
            	->first();

            	if($productos != null)
            	{
            		$productos->stock = $productos->stock+$producto->cantidad;
            		$productos->save();
            	}
            }

            \DB::commit();
        } catch (\Exception $e) {
        	\DB::rollBack();
            $data = array('estatus' => 501, 'mensaje' => $e->getMessage());
        }

        return $data;
	}


	public function calcular_envio($orden)
	{
		//calcular subtotal
		$subtotal = 0;
		foreach ($orden->productos as $key => $orden_producto)
		{
			$subtotal += $orden_producto->precio*$orden_producto->cantidad;
		}
		$configuracion = Configuracion::whereNotNull('costo_envio')->first();
		$orden->envio = $configuracion->costo_envio;

		//$total = $subtotal + $orden->envio;

		//$orden->total = $orden->subtotal + $orden->envio - $orden->descuento - $orden->cupon_cantidad;
		$iva = $subtotal*0.16;
 		$orden->iva = $iva;
		$orden->subtotal = $subtotal;
		$orden->total = $subtotal+$orden->envio-$orden->cupon_cantidad+$iva;
	    $orden->save();

    	return $orden;
	}
}