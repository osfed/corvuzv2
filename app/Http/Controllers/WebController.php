<?php

namespace App\Http\Controllers;

use App\Models\Blog;

use Illuminate\Http\Request;

class WebController extends Controller
{
    public function index()
    {
        return view('web.home');
    }

    public function contacto()
    {
        return view('web.contacto');
    }
    public function nosotros()
    {
        return view('web.nosotros');
    }
    public function servicios()
    {
        return view('web.servicios');
    }
    public function blog()
    {
        $blog = Blog::all();
        // return $blog;
        return view('web.blog')->with('blog',$blog);
    }
    public function blogDetalle($id)
    {
        $blogDetalle = Blog::where('slug',$id)->first();
        $ultimos = Blog::where('slug','!=',$id)->limit(3)->get();
        return view('web.blogDetalle')->with('blogDetalle',$blogDetalle)->with('ultimos',$ultimos);
    }
}
