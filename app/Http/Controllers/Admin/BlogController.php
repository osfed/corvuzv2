<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

use App\Models\Blog;
use Image;
use App\Utils\Utils;

class BlogController extends Controller
{
	public function index()
	{
		return view('admin.blog.index')->with('blog',Blog::all());
	}

	public function create()
	{
		return view('admin.blog.create');
	}

	public function store(Request $request)
	{
		DB::beginTransaction();
		try{
			$utils = new Utils;
			$slug = $utils->doSlug($request->input('titulo'));
			$check = Blog::where('slug',$slug)->first();
			if($check == null){
				$blog = new Blog;
				$blog->titulo = $request->input('titulo');
				$blog->keywords = $request->input('keywords');
				$blog->description = $request->input('description');
				$blog->intro = $request->input('intro');
				$blog->imagen = $request->hasFile('imagen') ? self::save_img($blog->imagen, $request->file('imagen'),840,600):$blog->imagen;
				$blog->post = $request->input('post');
				$blog->activo = $request->input('activo');
				$blog->slug = $slug;
				$blog->save();
			}else{
				DB::rollBack();
				$mensaje = 'Ya existe un evento con ese título';

				$alert = array(
					'estatus' => 'warning',
					'mensaje' => $mensaje
				);
				return back()->with('alert', $alert)->withInput();
			}

			DB::commit();
			return redirect()->route('blog.index');
		}catch(\Exception $e){
			DB::rollBack();
			$mensaje = 'Ocurrió un error intenta nuevamente, si el problema persiste contacta al administrador.';
			$data = array(
				'estatus' => 'error',
				'mensaje' => $mensaje
			);
			return back()->with('alert', $data);

		}
	}

	public function show($id)
	{
		//
	}

	public function edit($id)
	{
		return view('admin.blog.edit')->with('blog',Blog::find($id));
	}

	public function update(Request $request, $id)
	{
		DB::beginTransaction();
		try{
			$utils = new Utils;
			$slug = $utils->doSlug($request->input('titulo'));
			$check = Blog::where('slug',$slug)->first();
			// if($check == null){
				$blog = Blog::find($id);
				$blog->titulo = $request->input('titulo');
				$blog->keywords = $request->input('keywords');
				$blog->description = $request->input('description');
				$blog->intro = $request->input('intro');
				// $blog->imagen = $request->input('imagen');
				$blog->imagen = $request->hasFile('imagen') ? self::save_img($blog->imagen, $request->file('imagen'),840,600):$blog->imagen;
				$blog->post = $request->input('post');
				$blog->slug = $slug;
				$blog->save();
			// }else{
			// 	DB::rollBack();
			// 	$mensaje = 'Ya existe un evento con ese título';

			// 	$alert = array(
			// 		'estatus' => 'warning',
			// 		'mensaje' => $mensaje
			// 	);
			// 	return back()->with('alert', $alert)->withInput();
			// }

			DB::commit();
			return redirect()->route('blog.index');
		}catch(\Exception $e){
			DB::rollBack();
			$mensaje = 'Ocurrió un error intenta nuevamente, si el problema persiste contacta al administrador.';
			$data = array(
				'estatus' => 'error',
				'mensaje' => $mensaje
			);
			return back()->with('alert', $data);

		}
	}

	public function destroy($id)
	{
		$blog = Blog::find($id);
		$blog->delete();

		$data = array(
			'estatus' => 'success',
			'mensaje' => "Eliminado correctamente",
			'id' => $id
		);

		return response()->json($data);
	}

	static function save_img($current, $image,$w,$h){
		$pathFile = public_path().'/images/blog';
		if($current != '' && file_exists($pathFile.'/'.$current))
		{
			unlink($pathFile.'/'.$current);
		}

		$image_name = md5(time().rand(0,10)).$w.$h.".".$image->getClientOriginalExtension();
		$img = Image::make($image)
		->resize($w,$h)
		->save('images/blog/'.$image_name);

		return $image_name;
	}
}
