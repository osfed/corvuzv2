<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

class AccesoController extends Controller
{
    public function login(Request $request)
	{
		if($request->isMethod('post'))
		{
			$validator = Validator::make($request->all(), [
				'email' => 'required',
				'password' => 'required',
			]);

			if($validator->fails())
			{
				$alert = array(
					'estatus' => 'error',
					'mensaje' => 'Los campos son obligatorios'
				);
				return redirect()->route('login')
				->with('alert', $alert)
				->withInput();
			}
			$email = $request->input('email');
			$password = $request->input('password');

            $credentials = array('email' => $email, 'password' => $password);
			if (Auth::attempt($credentials))
			{
				return redirect()->route('admin_dashboard');
			}

			$alert = array(
				'estatus' => 'error',
				'mensaje' => 'No existe un usuario con esos datos'
			);
			return redirect()->route('login')
			->with('alert', $alert)
			->withInput();
		}else{
			return view('admin.login');
		}
	}
	public function dashboard()
	{
		return view('admin.dashboard');
	}
	public function salir()
	{
		Auth::logout();
		return redirect()->route('login');
	}
}
